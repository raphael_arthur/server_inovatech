<?php
/*
 *  This class has methods for sanitase inputs such as email and database input
 */
require_once "Database.class.php";

Class Sanitase{

	public function cleanInput($input) {
		$search = array(
				'@<script[^>]*?>.*?</script>@si',   // Strip out javascript
				'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
				'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
				'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		);

		return preg_replace($search, '', $input);
	}

    public function clearInsertion($string) {
        // Remove caracteres proibidos.
        $insercao = array("\\", "/", "<", ">", "=", "'", '"', "?", "!", ",", "[", "]", "{", "}", "(", ")", "^", "´", "`", ";", "~", "|", "*", "¬", "¨", "#", "%", "&", ":", "+", "$");
        $string = str_replace($insercao, "", $string);

	   return $string;
    }

    //Validade a String to be inserted in the current database
    public function ValidadeString($input) {
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = sanitize($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }

            $Database = new Database();
            $input  = $this->cleanInput($input);
            $output = $Database->escape_string($input);
            $Database->close();
        }
        return $output;
    }

    public function validateEmail($email){
        if (filter_var($email_a, FILTER_VALIDATE_EMAIL)) {
            return $email;
        } else {
            print "[]";
        }
    }

}
?>
