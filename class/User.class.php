<?php
require_once "Database.class.php";
require_once "Sanitase.class.php";
require_once "Security.class.php";

class User {
    private $password;
    private $id;
    private $Database;

    function __construct(){
        $this->Database = new Database();
    }

    private function incrementSigninAttempt() {
        $this->Database->query("UPDATE user SET attempts = attempts + 1 WHERE iduser = 1");
    }

    private function resetSigninAttempt() {
        $this->Database->query("UPDATE user SET attempts = 0 WHERE iduser = 1");
    }

    //Will return the string "true" if login was a sucess, and restart the attempts of password, if the user get the password wrong three times we will block the login.
    public function signin($mail, $password){
        $Sanitase = new Sanitase();
        $Security = new Security();

        $mail = $Sanitase->clearInsertion($mail);

        $this->id = $this->Database->selectSingleton("SELECT iduser FROM user WHERE mail = '$mail'","iduser");

        $signinAttempts = $this->Database->selectSingleton("SELECT attempts FROM user WHERE iduser = '$this->id'","attempts");

        if($signinAttempts > 3){
            //print "Your Account has been Blocked due too many passwords tentative. Please Contact an Administrator of Your System";
            //parse the info to the user in an JSON odbc_fetch_object
            return json_encode(array('userBlocked'=>'true'));
        } else {
            $password = $Sanitase->clearInsertion($Sanitase->ValidadeString($password));

            $dbpassword = $this->Database->selectSingleton("SELECT password FROM user WHERE iduser = '$this->id'","password");

            $privilegio = $this->Database->selectSingleton("SELECT privilegio FROM user WHERE iduser = '$this->id'","privilegio");

            if($Security->passwordVerify($dbpassword, $password)){
                $this->resetSigninAttempt();
           		session_start();
           		session_regenerate_id(true);

                $_SESSION['isSigned'] = "true";
                // $_SESSION['userToken'] = $Sanitase->cleanInput($Sanitase->cleanInput($Security->generateKey()));
                $_SESSION['userToken'] = $Security->generateKey();
                $_SESSION['REMOTE_ADDR'] = $Security->getUserIP();
                $_SESSION['HTTP_USER_AGENT'] = $Security->getUserAgent();

                $_SESSION["userID"] = $this->id;
                $_SESSION["mail"] = $mail;
                $_SESSION['privilegio'] = $privilegio;
                session_write_close();
                //print_r($_SESSION);
                //parse the info to the user in an JSON odbc_fetch_object
                return json_encode(array('userToken' => $_SESSION['userToken'], 
                        'isSigned' => $_SESSION['isSigned'], 
                        'REMOTE_ADDR' => $_SESSION['REMOTE_ADDR'],
                        'HTTP_USER_AGENT' => $_SESSION['HTTP_USER_AGENT'],
                        'userID' => $_SESSION["userID"],
                        'privilegio' => $_SESSION["privilegio"],
                        'mail' => $_SESSION["mail"]));
            } else {
                $this->incrementSigninAttempt();
                return json_encode(array('userToken' => '', 
                        'isSigned' => '', 
                        'REMOTE_ADDR' => '',
                        'HTTP_USER_AGENT' => '',
                        'userID' => '',
                        'privilegio' => '',
                        'mail' => ''));
            }
        }
    }
}
?>
