<?php
   
    require_once "../class/Database.class.php";
    require_once "../class/Security.class.php";
    
    $Database = new Database();
    $Security = new Security();
	
    if(isset($_POST["userToken"]) and isset($_POST["data"])){
        
        if($Security->authSession($_POST['userToken'])){
            //Sanitase this var as soon as possible
            $data = $_POST["data"];
            
            //This because we have only one page in the project
            $Statament = $Database->prepare("UPDATE pagina SET json = ? WHERE id = 'home' ");
            $Statament->bind_param('s', $data);
            $Statament->execute();
            
            if($Statament == true){
                print "true";
            } else {
                print "false";
            }
        }
        
    } else {
        print "Access Denied.";
    }
    
    $Database->close();
?>