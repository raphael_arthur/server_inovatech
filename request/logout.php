<?php
    //    This request check the user and destroy session Database
    require_once "../class/Security.class.php";

    $security = new Security();

    if($security->authSession($_POST["userToken"])){
        $_SESSION = array();
        
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        if(session_destroy()){
            print json_encode(array('loggedOut' => 'true'));
        } else {
            //parse the info to the user in an JSON odbc_fetch_object
            print json_encode(array('loggedOut' => 'false'));
        }
    } else {
        //print "Session does not Exists. Please, contact the administrator!";
        print json_encode(array('loggedOut' => '', 'msg' => 'Session does not Exists. Please, contact the administrator!'));
    }
?>