<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();

$eventId = '"'.$database->real_escape_string($_REQUEST["id_event"]).'"';
$scheduleId = '"'.$database->real_escape_string($_REQUEST["id_schedule"]).'"';
$userId = '"'.$database->real_escape_string($_REQUEST["user_id"]).'"';

$events = $database->selectSingleton("SELECT event_idevent FROM user_has_event WHERE user_iduser = '$userId'","event_idevent");

if($events != null)
{
    $present == 0;
    foreach ($events as $event)
    {
        if($event == $eventId)
        {
            $present = 1;
            break;
        }
    }
}

#if($events == 0 || $present == 0)
#{
    $insert_row = $database->query("INSERT INTO user_has_event (user_iduser, event_idevent) VALUES (".$userId.",".$eventId.")");
#}

$insert_row = $database->query("INSERT INTO user_has_schedule (user_iduser, schedule_idschedule) VALUES (".$userId.",".$scheduleId.")");
